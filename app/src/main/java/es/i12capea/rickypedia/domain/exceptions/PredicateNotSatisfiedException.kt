package es.i12capea.rickypedia.domain.exceptions

class PredicateNotSatisfiedException() : Throwable("Predicate not satisfied") {
}