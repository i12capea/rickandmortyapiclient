package es.i12capea.rickypedia.domain.entities

data class LocationShortEntity(
    val id: Int?,
    val name: String
)