package es.i12capea.rickypedia.data.api.models

data class Results <T> (
    val results: List<T>
)